#!/bin/sh
sudo sysctl kernel.nmi_watchdog=0
sudo perf stat -r 16 -d $1
sudo sysctl kernel.nmi_watchdog=1
