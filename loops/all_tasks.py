# https://stackoverflow.com/a/5443941/11924662
# https://docs.python.org/3.6/library/multiprocessing.html#module-multiprocessing.pool
from time import perf_counter
from functools import partial
from multiprocessing import Pool
from os import cpu_count
from math import log, log10


def A_array_populator_code_block( index ):
    A = log10( float( index+1 ) )
    return A

def B_array_populator_code_block( index ):
    B =   log( float( index+1 ) )
    return B


def sum_code_block( index, A, B ):
    assert len(A) == len(B)
    C = A[index] + B[index]
    return C


# nothing else is happening, but wanted to try async below
def head_process_worker():
    number_of_elements = 10000000
    list_of_indices = [ i for i in range(number_of_elements) ]
    #
    #  populate the input arrays
    pool0 = Pool( processes= cpu_count()-1 )
    resultA = pool0.map_async( A_array_populator_code_block, list_of_indices )
    resultB = pool0.map_async( B_array_populator_code_block, list_of_indices )
    A = resultA.get()
    B = resultB.get()
    pool0.close()
    pool0.join()
    #
    #  compute the output array
    pool1 = Pool( processes= cpu_count()-1 )
    result1 = pool1.map_async(
            partial( sum_code_block, A=A, B=B ), list_of_indices )
    pool1.close()
    pool1.join()
    return result1.get()


if __name__ == '__main__':
    t_initial = perf_counter()
    D = head_process_worker()
    #print( D )
    t_final = perf_counter()
    print( "\n elapsed time:", t_final-t_initial, "seconds\n" )



