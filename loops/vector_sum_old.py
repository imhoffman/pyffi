# https://stackoverflow.com/a/5443941/11924662
# https://docs.python.org/3.6/library/multiprocessing.html#module-multiprocessing.pool
from time import perf_counter
from functools import partial
from multiprocessing import Pool
from os import cpu_count
from math import log, log10


def sum_code_block( index, A, B ):
    assert len(A) == len(B)
    C = A[index] + B[index]
    return C


# nothing else is happening, but wanted to try async below
def head_process_worker():
    number_of_elements = 10000000
    list_of_indices = [ i for i in range(number_of_elements) ]
    #
    #  populate the input arrays
    # https://docs.python.org/3/library/functions.html#zip
    A, B = zip( *[
        [ log10( float(i+1) ), log( float(i+1) ) ] for i in list_of_indices ] )
    #
    #  compute the output array
    pool = Pool( processes= cpu_count()-1 )     # save one for Elvis
    result = pool.map_async(
            partial( sum_code_block, A=A, B=B ), list_of_indices )
    pool.close()
    pool.join()
    return result.get()


if __name__ == '__main__':
    t_initial = perf_counter()
    D = head_process_worker()
    #print( D )
    t_final = perf_counter()
    print( "\n elapsed time:", t_final-t_initial, "seconds\n" )



