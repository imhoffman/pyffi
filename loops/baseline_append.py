from time import perf_counter
from math import log, log10


def worker():
    number_of_elements = 100000000
    list_of_indices = [ i for i in range(number_of_elements) ]
    #
    A = [ log10( float(i+1) ) for i in list_of_indices ]
    B = [   log( float(i+1) ) for i in list_of_indices ]
    C = []
    for i in list_of_indices:
        C.append( A[i] + B[i] )
    return C


if __name__ == '__main__':
    t_initial = perf_counter()
    D = worker()
    #print( D )
    t_final = perf_counter()
    print( "\n elapsed time:", t_final-t_initial, "seconds\n" )


