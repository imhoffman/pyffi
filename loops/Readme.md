
## Pooling

In [`vector_sum_old.py`](vector_sum_old.py), the loading of the array is handled by the head process alone, in [`vector_sum.py`](vector_sum.py), the loading is its own pooled function, leaving only the `zip`ping to be done by the head process. The system monitor for the two scripts run one after another is shown below. The RAM usage is bothering me, so I need to look into explicitly sharing the arrays.

![monitor.png](monitor.png)


By removing the superfluous `zip` as in [`all_tasks.py`](all_tasks.py), the runtime drops by &sim;30%. By explicitly sharing the arrays in memory as in [`all_tasks_shared.py`](all_tasks_shared.py), the memory footprint and processor loading is more uniform.

![monitor2.png](monitor2.png)


For comparison, a single-core version ([`baseline.py`](baseline.py) or [`baseline_append.py`](baselineappend.py)) takes 2.5 times longer than when the loops are distributed over the cores.

![monitor3.png](monitor3.png)

