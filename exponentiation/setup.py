#  build and install the C code via Python
#  $ python3 setup.py install
from distutils.core import setup, Extension

def main():
    setup(name="cmath_imh",
          version="0.0.1",
          description="IMH interface for the C math library",
          author="IMH",
          author_email="cleriq@gmail.com",
          ext_modules=[Extension("cmath_imh", ["cmathmodule.c"])])

if __name__ == "__main__":
    main()

