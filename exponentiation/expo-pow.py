#  or, for testing with perf script, comment out `time` calls
#  $ ./perf.sh "python3 expo-pow.py"
from time import perf_counter

#  the different options for exponentiation implementation
#from cmath_imh import pow
from imhpow_module import imhpow as pow
#from math import pow

t_initial = perf_counter()

for _ in range(10000000):
    x = 1.1
    #x = pow(x,2.0)
    x = pow(x,2)
    #x = x * x              # to recognize looping overhead

t_final = perf_counter()

print( "\n elapsed time:", t_final-t_initial, "seconds\n" )

