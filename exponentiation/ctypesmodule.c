//  called from imhpow_module.py
//  for prior compilation and subsequent inclusion as a DLL
//  $ gcc -fPIC -shared -o imhpow.so ctypesmodule.c -lm
//  tail-called integer function is not compliant C ...
#include<math.h>


//  this function is called if a float exponent is passed
double
imhdoublepow ( double base, double exponent )
{
  return pow( base, exponent );
}


//  this function is called if an integer exponent is passed
//   this is an O(n) algorithm with no tail-call;
//   the interested reader may implement the O(log n) routine
//   https://en.wikipedia.org/wiki/Exponentiation_by_squaring
/*
double
imhintpow ( double base, int exponent )
{
  double pow_iter ( double accum
  if ( exponent == 0 ) return 1.0;
  else return base * imhintpow( base, exponent-1 );
}
*/


//   same as above but tail-called with an internal accumulator
//    not compliant C; use the commented version above if
//    you can't get it to compile
double
imhintpow ( double base, int exponent )
{
  double
  pow_iter ( double accum, int counter )
  {
    if ( counter == 0 ) {
      return accum;
    } else {
      return pow_iter( accum * base, counter - 1 );
    }
  }

  return pow_iter( 1.0, exponent );
} 

