import ctypes

#  paths can be super-problematic ...
_imhpow = ctypes.CDLL( './imhpow.so' )

#  input and output coersions
_imhpow.imhdoublepow.argtypes = ( ctypes.c_double, ctypes.c_double )
_imhpow.imhdoublepow.restype  =   ctypes.c_double
_imhpow.imhintpow.argtypes    = ( ctypes.c_double, ctypes.c_int )
_imhpow.imhintpow.restype     =   ctypes.c_double

#  this one method contains calls to both functions
def imhpow ( base, exponent ):
    #  not totally sure why this next line is needed
    global _imhpow

    #  test for integer; run appropriate C function
    if type( exponent ) == int:
        result = _imhpow.imhintpow(
                        ctypes.c_double( base ),
                        ctypes.c_int( exponent ) )
    else:
        result = _imhpow.imhdoublepow(
                        ctypes.c_double( base ),
                        ctypes.c_double( exponent ) )
    return float( result )

