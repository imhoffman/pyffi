//  $ sudo -H python3 setup.py install
//  $ python3
//  >>> import cmath_imh
//  >>> cmath_imh.__doc__
//  'IMH interface for the C math library'
//  >>> cmath_imh.__name__
//  'cmath_imh'
//  >>> cmath_imh.pow.__doc__
//  'Python interface for pow C library function'
//  >>> cmath_imh.pow( 4.5, 1.2 )
//  6.07932017334276
//  >>> cmath_imh.pow( 2, 1 )
//  2.0
//  >>> cmath_imh.pow( 1, 2 )
//  1.0
//  >>> cmath_imh.pow( 2, 2 )
//  4.0

#include<Python.h>
#include<math.h>


static PyObject *
c_pow( PyObject *self, PyObject *args )
{
  double base, exponent;
  double result;

  /* Parse arguments */
  if( !PyArg_ParseTuple( args, "dd", &base, &exponent ) ) {
      return NULL;
  }

  result = pow( base, exponent );

  return PyFloat_FromDouble( result );
}


static PyMethodDef
CMathIMHMethods[] = {
  {"pow", c_pow, METH_VARARGS, "Python interface for pow C library function"},
  {NULL, NULL, 0, NULL}    // dummy line to end array
};


static struct PyModuleDef
CMathIMHmodule = {
  PyModuleDef_HEAD_INIT,
  "cmath_imh",
  "IMH interface for the C math library",
  -1,
  CMathIMHMethods
};


PyMODINIT_FUNC
PyInit_cmath_imh( void )
{
  return PyModule_Create( &CMathIMHmodule );
}

